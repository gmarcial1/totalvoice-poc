﻿using System;
using System.IO;
using Newtonsoft.Json;
using TotalVoice;
using TotalVoice.Api;

namespace totalvoice_poc
{
    class Program
    {
        static void Main(string[] args)
        {
            var serializedNumbers = File.ReadAllText("./numbers.json");
            var deserializedNumbers = JsonConvert.DeserializeObject<string[]>(serializedNumbers);

            var client = new TotalVoiceClient("");
            var action = new Chamada(client);
 
            var json = new {
                numero_origem = deserializedNumbers,
                numero_destino = "999",
                gravar_audio = true,
                detecta_caixa = true,
                bina_inteligente = true,
            };

            
            var response = action.Ligar(json);
            Console.WriteLine(response);
        }
    }
}
